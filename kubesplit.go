package main

import (
	"fmt"
	"log"
	"os"
	"runtime"
	"strings"

	"io/ioutil"

	"gopkg.in/yaml.v2"
)

type Clusters struct {
	Cluster struct {
		InsecureSkipTLSVerify bool   `yaml:"insecure-skip-tls-verify"`
		CertificateAuthorityData string `yaml:"certificate-authority-data"`
		Server                string `yaml:"server"`
	} `yaml:"cluster"`
	Name string `yaml:"name"`
}

type Users struct {
	Name string `yaml:"name"`
	User struct {
		ClientCertificateData string `yaml:"client-certificate-data"`
		ClientKeyData         string `yaml:"client-key-data"`
		Username			  string `yaml:"username"`
		Password			  string `yaml:"password"`
	} `yaml:"user"`
}

type Kubecfg struct {
	APIVersion string `yaml:"apiVersion"`
	Clusters   []Clusters `yaml:"clusters"`
	Contexts []struct {
		Context struct {
			Cluster string `yaml:"cluster"`
			User    string `yaml:"user"`
		} `yaml:"context"`
		Name string `yaml:"name"`
	} `yaml:"contexts"`
	CurrentContext string `yaml:"current-context"`
	Kind           string `yaml:"kind"`
	Preferences    struct {
	} `yaml:"preferences"`
	Users []Users `yaml:"users"`
}

func main() {
	cfg := Kubecfg{}

	home := userHomeDir()
	data, err := ioutil.ReadFile(strings.Join([]string{home, ".kube", "config"}, "/"))

	if err != nil {
		log.Fatalf("error: %v", err)
	}

	err2 := yaml.Unmarshal(data, &cfg)
	if err2 != nil {
		log.Fatalf("error: %v", err2)
	}

	if len(os.Args) == 1 {
		fmt.Println("Contexts:")
		for i := 0; i < len(cfg.Contexts); i++ {
			name := cfg.Contexts[i].Name
			fmt.Printf("%v\n", name)
		}
	} else {
		var ix = -1
		for i, n := range cfg.Contexts {
			if n.Name == os.Args[1] {
				ix = i
				break
			}
		}

		if ix == -1 {
			fmt.Printf("Context named '%v' not found in kube-config\n\n", os.Args[1])
		} else {
			cluster := cfg.Contexts[ix].Context.Cluster
			user := cfg.Contexts[ix].Context.User

			x := Kubecfg{}
			x.APIVersion = "v1"
			x.Kind = "Config"
			x.Contexts = append(x.Contexts, cfg.Contexts[ix])
			if c, ok := findCluster(cluster, cfg); ok {
				x.Clusters = append(x.Clusters, c)
			}
			if u, ok := findUsers(user, cfg); ok {
				x.Users = u
			}

			d, err := yaml.Marshal(&x)
			if err != nil {
				log.Fatalf("error: %v", err)
			}
			fmt.Printf("%s\n", string(d))
		}
	}
}

func findCluster(cluster string, cfg Kubecfg) (Clusters, bool) {
	var ix = -1
	for i, n := range cfg.Clusters {
		if n.Name == cluster {
			ix = i
			break
		}
	}
	if ix != -1 {
		return cfg.Clusters[ix], true
	}
	return Clusters{}, false
}

func findUsers(user string, cfg Kubecfg) ([]Users, bool) {
	var users []Users
	for i, n := range cfg.Users {
		if strings.HasPrefix(n.Name, user) {
			users = append(users, cfg.Users[i])
		}
	}
	if len(users) != 0 {
		return users, true
	}
	return users, false
}

func userHomeDir() string {
	if runtime.GOOS == "windows" {
		home := os.Getenv("HOMEDRIVE") + os.Getenv("HOMEPATH")
		if home == "" {
			home = os.Getenv("USERPROFILE")
		}
		return home
	} else if runtime.GOOS == "linux" {
		home := os.Getenv("XDG_CONFIG_HOME")
		if home != "" {
			return home
		}
	}
	return os.Getenv("HOME")
}